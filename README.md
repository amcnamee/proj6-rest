# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## ACP controle times

Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

It implements the calculator at (https://rusa.org/octime_acp.html) with AJAX and Flask.

The first control closes an hour after the ride begins.

This calculator uses French rules: when a control is at 60 or less km from the start, it closes at 20km/hr + 1hr.

Otherwise, the calculator uses this table (from rusa.org)

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
| 0 - 200               | 15                    | 34                    |
| 200 - 400             | 15                    | 32                    |
| 400 - 600             | 15                    | 30                    |
| 600 - 1000            | 11.428                | 28                    |

The last control can be at or less than 20% greater than the length of the brevet and closes at:

* 13:30 for 200 KM

* 20:00 for 300 KM

* 27:00 for 400 KM

* 40:00 for 600 KM

* 75:00 for 1000 KM  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link.

* Each time a distance is filled in, the corresponding open and close times are filled in.

## Functionality added


## Functionality you will add

This project has the following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* RESTful service to expose what is stored in MongoDB for the following three basic APIs:
    * "http://<host:port>/listAll" returns all open and close times in the database
    * "http://<host:port>/listOpenOnly" returns open times only
    * "http://<host:port>/listCloseOnly" returns close times only

* Designs two different representations: one in csv and one in json. For the above, JSON should be your default representation for the above three basic APIs.
    * "http://<host:port>/listAll/csv" returns all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" returns open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" returns close times only in CSV format

    * "http://<host:port>/listAll/json" returns all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" returns open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" returns close times only in JSON format

* Adds a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" returns top 3 open times only (in ascending order) in CSV format
    * "http://<host:port>/listOpenOnly/json?top=5" returns top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" returns top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" returns top 4 close times only (in ascending order) in JSON format

* Consumer programs to use the service.

## Test cases

1) When submit button is pressed and there are values present in the form, those values are
added to the database.

2) When submit button is pressed and there are no values present in the form, no values are added to
the database.

3) When display button is pressed and there are values present in the database, the page redirects to display those values.

4) When display button is pressed and there are no values present in the database, the page does
not redirect.

## Author

Audra McNamee

amcnamee@uoregon.edu
