<html>
    <head>
        <title>CIS 322 REST-api: Brevet control list</title>
    </head>

    <body>
        <h1>List of brevet controls</h1>
        <ul>
          <!-- this is an example - expose the database -->
            <?php
            //$json = file_get_contents('http://brevet-service/listAll/csv');
            $json = file_get_contents('http://brevet-service/listAll');
            //$json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
            $array = json_decode(json_encode($obj), true);

            foreach ($array as $item){
              echo "<li>";
              print_r($item);
              echo "</li>";
            }

            ?>
        </ul>
    </body>
</html>
