import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

import os
from pymongo import MongoClient

from flask_restful import Resource, Api

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client.drop_database('tododb') # deletes database every time
db = client.tododb

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.route('/_insert_value', methods=['POST'])
def new():
    if request.args.get('error', type=int) != 1:
        item_doc = {
            'km': int(request.form['km']),
            'miles': request.form['miles'],

            'brevet_len': request.form['brevet_len'],
            'start_date': request.form['start_date'],
            'start_time': request.form['start_time'],

            'open_time': request.form['open_time'],
            'close_time': request.form['close_time']
        }
        db.tododb.insert_one(item_doc)

    # todo state that the database has an item

    return redirect(url_for('index'))

@app.route('/show')
def show():
    _items = db.tododb.find()
    items = [item for item in _items]
    if items != []:
        return render_template('posting.html', items=items)

    return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects four URL-encoded arguments: the number of kilometers, the length of the brevet, and the start time and date.
    """
    # Using Jquery ID selectors and then passing the date and time to the server as separate arguments
    #date_time_string = date + ' ' + time + ':00'
    #date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')

    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    brevet_len = request.args.get('brevet_len', 999, type=int)
    start_date = request.args.get('start_date', type=str)
    start_time = request.args.get('start_time', type=str)
    # turn the date and time into an arrow
    start_arrow = arrow.get(start_date + ' ' + start_time + ':00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    app.logger.debug(start_arrow)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    result = {"open": None, "close": None, "error":0, "message": None}

    if km < 0:
        result["error"] = 1
        result["message"] = "Error: negative"

    elif (km - brevet_len)/brevet_len > .2:
        result["error"] = 1
        result["message"] = "Error: too far"

    else:
        open_time = acp_times.open_time(km, brevet_len, start_arrow)
        close_time = acp_times.close_time(km, brevet_len, start_arrow)
        result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


#############
#
# RESTful API
#
#############

@app.route("/listAll")
def get_all():
    _items = db.tododb.find().sort("km")
    items = [item for item in _items]

    return flask.jsonify(open_close_dict(items))
    # return render_template('posting.html', items=items)

@app.route("/listOpenOnly")
def get_open(type="json"):
    _items = db.tododb.find()
    items = [item for item in _items]

    return flask.jsonify(open_close_dict(items, close=False))

@app.route("/listCloseOnly")
def get_close():
    _items = db.tododb.find()
    items = [item for item in _items]

    return flask.jsonify(open_close_dict(items, open=False))

@app.route("/listAll/<type>")
def get_all_type(type="json"):
    top_num = int(flask.request.args.get('top', default=-1))
    _items = db.tododb.find().sort("km")
    items = [item for item in _items]

    times = open_close_dict(items)
    if top_num != -1:
        times = times[:top_num]

    if type == "csv":
        return flask.jsonify(csvify(times))
    else:
        return flask.jsonify(times)
    # return render_template('posting.html', items=items)

@app.route("/listOpenOnly/<type>")
def get_open_type(type="json"):
    top_num = int(flask.request.args.get('top', default=-1))
    _items = db.tododb.find()
    items = [item for item in _items]

    times = open_close_dict(items, close=False)
    if top_num != -1:
        times = times[:top_num]


    if type == "csv":
        return flask.jsonify(csvify(times))
    else:
        return flask.jsonify(times)

@app.route("/listCloseOnly/<type>")
def get_close_type(type="json"):
    top_num = int(flask.request.args.get('top', default=-1))
    _items = db.tododb.find()
    items = [item for item in _items]

    times = open_close_dict(items, open=False)
    if top_num != -1:
        times = times[:top_num]

    if type == "csv":
        return flask.jsonify(csvify(times))
    else:
        return flask.jsonify(times)

def open_close_dict(li, open=True, close=True):
    retlist = []
    # key: km, val: open and close times
    for i in li:
        item = {}
        if open:
            item["open_time"] = i["open_time"]
        if close:
            item["close_time"] = i["close_time"]
        item["km"] = i["km"]

        retlist.append(item)

    return retlist

def csvify(li):
    retstr = ""
    for item in li:
        retstr += f"{item['km']},"
        try:
            retstr += f"{item['open_time']},"
        except:
            pass
        try:
            retstr += f"{item['close_time']},\n"
        except:
            pass
    return retstr[:-2]

################

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
